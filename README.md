# Soft-Starter

#### PROJETO 1: SOFTSTARTER
Para obter uma partida suave deve-se diminuir a tensão aplicada ao motor no instante da partida (essa tensão é denominada de tensão de pedestal - Vp) e aumentá-la com o tempo, conforme a carga conectada ao seu eixo, até se atingir a
tensão nominal Vn.
Para obter a característica desejada do controle de partida, são utilizados dois tiristores para cada fase, um para cada semiciclo. Estes devem ser acionados com um ângulo de disparo α que vai variando até seu ângulo mínimo conforme mostra a
figura abaixo.

![Screenshot](fig/fig1.PNG)

#### Características do Sistema
A curva característica de um sistema de controle de aceleração e desaceleração pode ser vista na figura 2, onde uma aceleração progressiva é imposta ao motor através de uma rampa no instante da partida do motor, e uma desaceleração progressiva é responsável pela frenagem ou parada do motor.

![Screenshot](fig/fig2.PNG)

O controle dos disparos dos tiristores pode ser realizado com circuitos analógicos discretos ou microprocessados.
O sistema deve apresentar, no mínimo:
* Dois botões: START e STOP;
* Na partida e no desligamento do motor deve-se ajustar o tempo entre 5s e 15s;
* Ângulo da tensão de pedestal deve ser de 120 graus;
* Um LED de fim de rampa de aceleração deverá sinalizar o momento em que o motor atingir a tensão nominal;
* Um LED de fim de rampa de desaceleração.

Para a carga foi escolhido um motor universal (figura 3) que possui baixa potência (aproximadamente 100 W). Este motor pode ser ligado na rede de corrente contínua ou na rede AC, uma vez que os campos no estator e no rotor se invertem simultaneamente, quando a corrente inverte o seu sentido, fazendo com que a rotação fique sempre a mesma.

![Screenshot](fig/fig3.PNG)

O esquema de ligação do motor é apresentado na figura 4

![Screenshot](fig/fig4.PNG)