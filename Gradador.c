/*----------------------------------------------------------------------------*/
// Teste de Fun��es de prot�tipo
/*----------------------------------------------------------------------------*/
void ConfigInterrupt()
{
	//Timer1 Registers Prescaler= 1 - TMR1 Preset = 21104 - Freq = 180.05 Hz - Period = 0.005554 seconds
	setup_timer_1(T1_INTERNAL|T1_DIV_BY_1);
	enable_interrupts(INT_TIMER1);
	
	RefreshMemoryStep();
}
/*----------------------------------------------------------------------------*/
// Teste de Fun��es de prot�tipo
/*----------------------------------------------------------------------------*/
void RefreshMemoryStep()
{
	uint8_t temp;
	temp = MAX_N - Flag8[ADDR_RAMPA];
	temp *= STEP_STEP;
	temp += MIN_STEP;
	Flag8[ADDR_STEP] = temp;
}
/*----------------------------------------------------------------------------*/
// Teste de Fun��es de prot�tipo
/*----------------------------------------------------------------------------*/
#inline
void sincInterrupt()
{
	#define H2L_CORRECTION	4
	//Limpa a interrup��o
	if(SINC) {
		//Erro de leitura para borda de descida de +600us
		// 600us/125us = 4 passos
		set_timer1(DelayTrigger);
		setDelayTrigger();
	}
	else{
		set_timer1(DelayTrigger-H2L_CORRECTION);
	}

	//Liga o Timer 1
	enable_interrupts(INT_TIMER1);
	TRIAC_2 = !TRIAC_2;
}
/*----------------------------------------------------------------------------*/
// Teste de Fun��es de prot�tipo
/*----------------------------------------------------------------------------*/
#inline
void timerInterrupt(){
	if(TRIAC_1){
		TRIAC_1 = OFF;
		disable_interrupts(INT_TIMER1);
	}
	else{
		if(Estado == SENT1){
			TRIAC_1 = ON;
		}
		else if(Estado == SENT2){
			TRIAC_1 = ON;
		}
		set_timer1(PRESET_GATE_TMR1);
	}
}
/*----------------------------------------------------------------------------*/
// Teste de Fun��es de prot�tipo
/*----------------------------------------------------------------------------*/
#inline
void setDelayTrigger(){
	if(Estado == SENT1){
		//PEDESTAL_TIME		21104 - 49
		if(DelayTrigger > (PEDESTAL_TIME - Flag8[ADDR_STEP]))
			DelayTrigger -= Flag8[ADDR_STEP];
		else{
			DelayTrigger = PEDESTAL_TIME;
			Destino = PARA;
		}
	}
	else if(Estado == SENT2){
		//UM_GRAUS_TIME		65166 + 49
		if(DelayTrigger < (UM_GRAUS_TIME + Flag8[ADDR_STEP]))
			DelayTrigger += Flag8[ADDR_STEP];
		else{
			DelayTrigger = UM_GRAUS_TIME;
			Destino = LIGA_FULL;
		}
	}
}
/*----------------------------------------------------------------------------*/
// Teste de Fun��es de prot�tipo
/*----------------------------------------------------------------------------*/
void setStep(bool inc){

	if(inc){
		if(Flag8[ADDR_RAMPA] < MAX_N){
			Flag8[ADDR_RAMPA] ++;
			IndBuzzer(Flag8[ADDR_RAMPA], 300);
		}
	}
	else{
		if(Flag8[ADDR_RAMPA] > 0){
			Flag8[ADDR_RAMPA] --;
			IndBuzzer(Flag8[ADDR_RAMPA], 300);
		}
	}

	RefreshMemoryStep();
}
/*----------------------------------------------------------------------------*/