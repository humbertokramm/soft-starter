/*----------------------------------------------------------------------------*/
// DEFINI��ES
/*----------------------------------------------------------------------------*/
//Timer1 Registers Prescaler= 1 - TMR1 Preset = 61536 - Freq = 2000.00 Hz - Period = 0.000500 seconds
#define PRESET_GATE_TMR1	61536
//Timer1 Registers Prescaler= 1 - TMR1 Preset = 21104 - Freq = 180.05 Hz - Period = 0.005554 seconds
//Timer1 Registers Prescaler= 1 - TMR1 Preset = 65166 - Freq = 21621.62 Hz - Period = 0.000046 seconds
// Delta da rampa = 44062
// step 125ns
// tempo de acelera��o de 5s a 15s
// 5s = 300 ciclos, ou seja, no m�ximo 300 passos
// isso implica em decrementos de 44062/300 = 146.87 = 147
// 15s = 900 ciclos, ou seja, 900 passos
// isso implica em decrementos de 44062/900 = 48.958 = 49
// Delta incrementos = 147-49 = 98
// step do step = 98/10 = 9.8 = +-10
// 
#define	PEDESTAL_TIME		21104	//120�, que em 60Hz � igual a 5.55ms
#define	UM_GRAUS_TIME		65166	//1�, que em 60Hz � igual a 46.296us
#define	MAX_STEP			140//147	//5 segundos
#define	MIN_STEP			45//49	//15 segundos
#define	STEP_STEP			10
#define	MAX_N				10
#define	START_STEP			0 // MAX_N - START_STEP * STEP_STEP + MIN_STEP
/*----------------------------------------------------------------------------*/
// DECLARA��O DE VARIAVEIS GLOBAIS
/*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
// Prot�tipos
/*----------------------------------------------------------------------------*/
void ConfigInterrupt();
void RefreshMemoryStep();
#inline
void sincInterrupt();
#inline
void timerInterrupt();
#inline
void setDelayTrigger();
void setStep(bool inc);
/*----------------------------------------------------------------------------*/
// DECLARA��O DE VARIAVEIS GLOBAIS
/*----------------------------------------------------------------------------*/
uint16_t
	//StepLoad,
	DelayTrigger = PEDESTAL_TIME;
/*----------------------------------------------------------------------------*/